﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrappers
{
    //Example https://www.vulnhub.com/?page=1
    public class VulnHubScrapper : AScrapper
    {
        protected int _numberPage=0;
        public int NumberPage 
        {
            get { return _numberPage; }
            set 
            { 
                _numberPage = value;
                UrlPetition = String.Format("{0}/?page={1}", BaseUrl, _numberPage);
            }
        }
        private string BufferHtml { get; set; } = String.Empty;
        public string UrlPetition { get; protected set; }=String.Empty;
        public bool isFinish { get; protected set; } = false;
        public VulnHubScrapper()
        {
            base.BaseUrl = "https://www.vulnhub.com";
            NumberPage = 99999999;
        }
        public override async Task<string> GetPageHtml(string url)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("User-Agent", "C# program");
                string body = await client.GetStringAsync(url);
                if (url.Contains("/?page="))
                {
                    HtmlDocument htmlDocument=new HtmlDocument();
                    htmlDocument.LoadHtml(body);
                    string tittle = htmlDocument.DocumentNode.SelectSingleNode("//head/title").InnerText;
                    if (BufferHtml.Equals(tittle))
                    {
                        throw new Exception();
                    }
                    BufferHtml = tittle;
                    
                }
                
                return body;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public override List<string>? GetMachineLink(string html)
        {
            try
            {
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(html);

                var tagsAhtml = htmlDocument.DocumentNode.SelectNodes("//div[@class='card']/a");
                List<string> links = (from link in tagsAhtml
                        select link.Attributes.Where(e=>e.Name=="href").First().Value).ToList();

                return links;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override async Task<MachineModel?> GetMachine(string link)
        {
            try
            {
                MachineModel machineModel = new MachineModel();
                
                string finalUrlMachine = String.Format("{0}{1}", base.BaseUrl, link);
                string body = await GetPageHtml(finalUrlMachine);
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(body);

                machineModel.Url = finalUrlMachine;
                List<HtmlNode> infoRelease = htmlDocument.DocumentNode.SelectNodes("//div/div[@id='release']/ul/li").ToList();
                foreach (HtmlNode li in infoRelease)
                {
                    string field = String.Empty;
                    string text = String.Empty;
                    if (li.InnerText.Split(":").Length>=2)
                    {
                        field = li.InnerText.Split(":")[0];
                        text = li.InnerText.Replace(String.Format("{0}:", field),
                            String.Empty);
                    }
                    if (field.Contains("Name"))
                    {
                        machineModel.Name = text;
                    }
                    else if(field.Contains("Date")&& DateOnly.TryParse(text, out var date))
                    {
                        machineModel.Date = date.ToString();
                    }
                    else if (field.Contains("Author"))
                    {
                        machineModel.Author = text;
                    }
                }

                List<HtmlNode> infoDowload = htmlDocument.DocumentNode.SelectNodes("//div/div[@id='download']/ul/li/a[@href]").ToList();
                infoDowload.ForEach(e =>
                {
                    string href = e.Attributes.First(e => e.Name == "href").Value;
                    machineModel.UrlDownload.Add(href);
                });

                List<HtmlNode> infoDescription = htmlDocument.DocumentNode.SelectNodes("//div[@id='description']//p").ToList();
                infoDescription.ForEach(e =>
                {
                    string text = e.InnerText;
                    
                    if (text.Contains("Difficulty:"))
                    {
                        machineModel.Dificulty = text.Replace("Difficulty:", String.Empty);
                    }
                    else
                    {
                        machineModel.Description = text;
                    }
                    
                });

                return machineModel;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override async Task<List<MachineModel>> GetMachines()
        {
            List<MachineModel> machines = new List<MachineModel>();
            
            try
            {
                string body = await GetPageHtml(this.UrlPetition);
                List<string>? relativePaths = GetMachineLink(body);
                if (relativePaths != null)
                {
                    foreach (string path in relativePaths)
                    {
                        MachineModel? machineModel = await GetMachine(path);
                        if (machineModel != null)
                        {
                            machines.Add(machineModel);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            
            return machines;
        }

        public override async Task<List<MachineModel>> GetAllMachines()
        {
            
            List<MachineModel> machineModelsToReturned = new List<MachineModel>();
            do
            {
                try
                {
                    List<MachineModel> machineModels = await GetMachines();
                    machineModelsToReturned.AddRange(machineModels);
                    NumberPage++;
                }
                catch (Exception)
                {
                    isFinish= true;
                }
            } while (!isFinish);
            return machineModelsToReturned;
        }
    }
}
