﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrappers
{
    public class MachineModel
    {
        public string Name { get; set; }=String.Empty;
        public string Url { get; set; } = String.Empty;
        public string Author { get; set; } = String.Empty;
        public List<string> UrlDownload{ get; set; } = new List<string>();
        public string Date { get; set; } = String.Empty;
        public string Description { get; set; } = String.Empty;
        public string Dificulty { get; set; } = String.Empty;

    }
}
