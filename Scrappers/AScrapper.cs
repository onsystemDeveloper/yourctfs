﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrappers
{
    public abstract class AScrapper:IScrapper
    {
        public string BaseUrl { get; protected set; }=String.Empty;

        public abstract Task<List<MachineModel>> GetAllMachines();
        public abstract Task<MachineModel?> GetMachine(string link);
        public abstract List<string>? GetMachineLink(string html);
        public abstract Task<List<MachineModel>> GetMachines();
        public abstract Task<string> GetPageHtml(string url);

    }
}
