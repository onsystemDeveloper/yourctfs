﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scrappers
{
    public interface IScrapper
    {
        public Task<string> GetPageHtml(string url);
        public List<string>? GetMachineLink(string html);
        public Task<MachineModel?> GetMachine(string link);
        public Task<List<MachineModel>> GetMachines();

        public Task<List<MachineModel>> GetAllMachines();
    }
}
